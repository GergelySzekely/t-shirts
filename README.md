### Some t-shirt/sticker designs

Here I collect some of my t-shirt/sticker designs. 
Please, feel free to use and/or improve them as you like (respecting the licenes of the used materials, such as the [GNU Head](https://www.gnu.org/graphics/heckert_gnu)). 


[ChangeToFreeSoftware-AskMeHow.svg](https://gitlab.com/GergelySzekely/t-shirts/blob/master/ChangeToFreeSoftware-AskMeHow.svg) is based only on [GNU Head](https://www.gnu.org/graphics/heckert_gnu) by Aurelio A. Heckert. 

[fully-free-GNULinux-libre.svg](https://gitlab.com/GergelySzekely/t-shirts/blob/master/fully-free-GNULinux-libre.svg) is based on [tshirt1.svg](https://gitlab.com/kannanvm/gnupromo/blob/master/tshirt1.svg) by Kannan V M using logos of the [Uruk Project](https://commons.wikimedia.org/wiki/File:Logo_of_Uruk_Project.svg), [Parabola Gnu Linux-libre](https://en.wikipedia.org/wiki/File:Parabola_Gnu_Linux-libre.svg), [Guix System Distribution](https://en.wikipedia.org/wiki/File:Guix_System_Distribution_logo.svg), and [Trisquel](https://en.wikipedia.org/wiki/File:Logo-Trisquel.svg).